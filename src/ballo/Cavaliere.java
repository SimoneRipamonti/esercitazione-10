package ballo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Cavaliere implements Runnable {
	private final SalaDaBallo salaDaBallo;
	private final Random random;
	private final String id;

	public Cavaliere(String id, Random random, SalaDaBallo salaDaBallo) {
		this.id = id;
		this.random = random;
		this.salaDaBallo = salaDaBallo;
	}

	public void iterate() {
		List<Dama> dame = new ArrayList<Dama>(this.salaDaBallo.getDame());
		try {
			Thread.sleep(5000 * random.nextInt(5));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Collections.shuffle(dame);
		Dama d = dame.get(0);
		if(d.getStato()==Stato.IN_COPPIA){
			System.out.println("Sono "+this.id+" mi metto in attesa di "+d.getNome());
			d.attendi();
			System.out.println("Sono "+this.id+" mi hanno svegliato "+d.getNome()+" si � lasciata");
		}
		if (d.rispondi()) {
			System.out.println("Sono " + this.id + " la mia dama e' "
					+ d.getNome());
			try {
				Thread.sleep(5000 * random.nextInt(5));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			d.rilascia();
			System.out.println("Sono " + this.id + " ho lasciato "
					+ d.getNome());
		} else {
			System.out.println("Sono " + this.id + " sono stato rifiutato da "
					+ d.getNome());
		}

	}

	@Override
	public void run() {
		while (true) {
			this.iterate();
		}
	}
}
