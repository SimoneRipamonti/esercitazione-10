package ballo;

import java.util.Random;

public class Dama {
	private final String nome;
	private Stato stato;
	private Random random;
	
	public Dama(String dama, Random random){
		this.nome=dama;
		this.random=random;
		this.stato=Stato.LIBERA;
	}
	
	public String getNome(){
		return this.nome;
	}
	
	public synchronized void rilascia(){
		this.stato=Stato.LIBERA;
		this.notifyAll();
	}
	
	public synchronized boolean rispondi(){
		if(this.stato==Stato.IN_COPPIA){
			return false;
		}
		boolean result = this.random.nextBoolean();
		if(result){
			this.stato=Stato.IN_COPPIA;
		}
		return result;
	}
	
	public synchronized Stato getStato(){
		return this.stato;
	}
	
	public synchronized void attendi(){
		while(this.getStato()==Stato.IN_COPPIA){
			try {
				this.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
