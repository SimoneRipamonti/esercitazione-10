package ballo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SalaDaBallo {
	private List<Dama> dame;

	public SalaDaBallo(){
		dame = new ArrayList<Dama>();
		dame.add(new Dama("una mega cacca", new Random()));
		dame.add(new Dama("uno stronzo", new Random()));
		dame.add(new Dama("una sorpresa", new Random()));
		dame.add(new Dama("un ricordino", new Random()));
		new Thread(new Cavaliere("Fulvio", new Random(), this)).start();
		new Thread(new Cavaliere("Luca", new Random(), this)).start();
		new Thread(new Cavaliere("Alberto", new Random(), this)).start();
		new Thread(new Cavaliere("Simone", new Random(), this)).start();


	}
	public List<Dama> getDame() {
		return dame;
	}

	public static void main(String[] args) {
		new SalaDaBallo();

	}
	

}
