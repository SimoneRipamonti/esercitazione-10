package ballo;
/**
 * Stato di una dama
 * @author Simone
 *
 */
public enum Stato {
	/**
	 * La dama � libera
	 */
	LIBERA,
	/**
	 * La dama � accoppiata
	 */
	IN_COPPIA;
}
