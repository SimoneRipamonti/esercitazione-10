package gara;

public class Animale implements Runnable {

	private int posizione;
	private int lunghezzaPasso;
	private Race race;

	public Animale(Race race, int lunghezzaPasso) {
		this.posizione = 0;
		this.lunghezzaPasso = lunghezzaPasso;
		this.race = race;
	}

	@Override
	public void run() {
		boolean arrivato = false;
		while (!arrivato) {
			posizione = posizione + lunghezzaPasso;
			if (posizione > this.race.getDistanza()) {
				System.out.println("Sono il thread: "
						+ Thread.currentThread().getName() + " sono arrivato");
				arrivato = true;
			} else {
				System.out.println("Sono il thread: "
						+ Thread.currentThread().getName() + " posizione: "
						+ this.posizione);
			}
		}

	}

}
