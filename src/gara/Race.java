package gara;

public class Race {
	private int distanza;
	private static final int CONIGLIO_LUNGHEZZA_P = 5;
	private static final int TARTARUGA_LUNGHEZZA_P = 3;

	public Race(int distanza) {
		this.distanza = distanza;
	}

	public int getDistanza() {
		return distanza;
	}

	public static void main(String[] args) throws InterruptedException {
		Race race = new Race(100);
		Thread coniglio = new Thread(new Animale(race, CONIGLIO_LUNGHEZZA_P),
				"coniglio");
		Thread tartaruga = new Thread(new Animale(race, TARTARUGA_LUNGHEZZA_P),
				"tartaruga");
		coniglio.start();
		tartaruga.start();
	}
}
